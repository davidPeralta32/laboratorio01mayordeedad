package com.example.laboratorio01mayordeedad

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnProcesar: Button = findViewById(R.id.btnProcesar)
        val tvResultado: TextView = findViewById(R.id.tvResultado)

        btnProcesar.setOnClickListener {
            val tvEdad : Int = findViewById<EditText>(R.id.etEdad).text.toString().toInt()
            if (tvEdad >= 18){
                tvResultado.text = "Usted es mayor de edad"
            }else{
                tvResultado.text = "Usted es menor de edad"
            }
        }

    }
}